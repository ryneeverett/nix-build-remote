#!/usr/bin/env bash

pipestrip() {
  read str
  echo "$str" | tr -d '\r\n'
}

testLocalContainer() {
  assertEquals "bob" "$(local-shell whoami | pipestrip)"
}

testRemoteContainer() {
  assertEquals "alice" "$(remote-shell whoami | pipestrip)"
}

testExample() {
  rand=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 13)
  examplecmd=$(generate-example 0 "$rand")
  eval "$examplecmd"
  # shellcheck disable=SC2086
  result=$(local-shell cat result | pipestrip)
  assertNotEquals "$rand" "$result"
  # Make sure the build has completed before running again.
  sleep 1
  eval "$examplecmd"
  # shellcheck disable=SC2086
  result=$(local-shell cat result | pipestrip)
  assertEquals "$rand" "$result"
}

testExamplewithPassword() {
  ## User should be able to use password authentication rather than having a keyfile configured if they prefer.

  # Copy Local Container
  localcontainer='nix-build-remote-dev-local-instance-password'
  lxc delete $localcontainer --force
  # shellcheck disable=SC2064
  trap "lxc delete $localcontainer --force" EXIT
  lxc copy nix-build-remote-dev-local-instance $localcontainer --ephemeral
  lxc start $localcontainer
  # shellcheck disable=SC2064
  trap "export LOCAL=$(printenv LOCAL)" RETURN
  LOCAL=$(get-container-ip "$localcontainer")
  export LOCAL

  # Remove Identity and Existing Results
  local-shell 'mkdir -p ~/.ssh && cat << EOF > ~/.ssh/config
StrictHostKeyChecking=accept-new
EOF'
  local-shell 'rm ~/result'

  # Test Example
  # XXX I'm just hardcoding this one for now because escaping is too much of a pain. The downside is that without creating unique derivations tests could misleadingly pass when run with pre-existing containers.
  nixBuildRemotePassword() {
    local-shell 'expect << EOF
    spawn sh /home/bob/run-nix-build-remote-with-password.sh
    expect "Password:"
    send -- "obscurity\r"
    expect eof
EOF'
  }
  # shellcheck disable=SC2016
  local-shell 'cat << EOF > /home/bob/run-nix-build-remote-with-password.sh
#!/usr/bin/env sh
/home/bob/nix-build-remote/nix-build-remote.sh -E '\''(with import <nixpkgs> { }; runCommand "foo" {} "echo notsorandom > \$out")'\''
EOF'
  nixBuildRemotePassword
  # shellcheck disable=SC2086
  result=$(local-shell cat result | pipestrip)
  # If the result was already built on the remote container it will already be copied to the local container.
  if [ "$result" != "notsorandom" ]; then
    nixBuildRemotePassword
    # shellcheck disable=SC2086
    result=$(local-shell cat result | pipestrip)
  fi
  assertEquals "notsorandom" "$result"
}

testExampleInProgress() {
  ## nix-build-remote should notify when build is already in progress
  examplecmd=$(generate-example 5)

  output=$(eval "$examplecmd")
  assertNotContains "$output" "Build is already in progress!"
  output=$(eval "$examplecmd")
  assertContains "$output" "Build is already in progress!"
}

. shunit2
