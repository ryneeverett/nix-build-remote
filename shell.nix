{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    (import ./. { inherit pkgs; })

    lxd
    nixos-generators
    shunit2
  ];

  shellHook = builtins.readFile ./dev/shellhook.sh;
}
