#!/usr/bin/env sh
set -e

if [ "$NIX_BUILD_REMOTE" = "" ]; then
  echo "NIX_BUILD_REMOTE environment variable must be assigned '<user>@<address>' of the remote build machine."
  exit 1
fi

builder="$NIX_BUILD_REMOTE"
drv=$(nix-instantiate "$@" 2>/dev/null)

holler() {
  echo "$(tput bold)$*$(tput sgr0)"
}

symlinkresult() {
  nix-build "$@" >/dev/null
  exit 0
}

if nix-store --max-jobs 0 --realise "$drv" 2>/dev/null; then
  holler "Output already exists locally."
  symlinkresult "$@"
fi

tmpdir=$(mktemp -d)
trap 'rm -rf $tmpdir' EXIT
ctrlpath="$tmpdir/controlsocket"
ssh -Nf -o ControlMaster=yes -o "ControlPath=$ctrlpath" "$builder"

remote() {
  # shellcheck disable=SC2086
  ssh -t -o "ControlPath=$ctrlpath" -o LogLevel=QUIET $NIX_SSHOPTS "$builder" "$@"
}

copyclosure() {
  NIX_SSHOPTS="$NIX_SSHOPTS -o ControlPath=$ctrlpath" nix-copy-closure "$@"
}

if remote nix-store --max-jobs 0 --realise "$drv" >/dev/null; then
  holler "Output already built on remote. Copying to host..."
  outpath=$(remote nix-build --no-out-link "$drv" | tr -d '\r')
  remote nix sign-paths --key-file /etc/signingkey.pem "$outpath"
  copyclosure --from "$builder" "$outpath"
  symlinkresult "$@"
else
  holler "Copying closure to remote..."
  copyclosure --to "$builder" "$drv" 2>/dev/null
  holler "Beginning build on remote..."
  # shellcheck disable=SC2086
  ssh -f -o "ControlPath=$ctrlpath" $NIX_SSHOPTS "$builder" "sh -c '
  rm buildStderr 2&>/dev/null && echo \"WARNING: removing dangling buildStderr\"
  mkfifo buildStderr

  nohup nix-build --no-out-link \"$drv\" 1>/dev/null 2>buildStderr &

  n=1
  while read line; do
    echo \$line
    if [ \$n -ge 3 ]; then
      if [ \"\$line\" = \"waiting for locks or build slots...\" ]; then
        echo \"$(tput bold)Build is already in progress!$(tput sgr0)\"
      fi
      break
    fi
    n=\$((\$n+1))
  done<buildStderr

  rm buildStderr
  '"
  exit 0
fi

holler "nix-build-remote logic failure"
exit 1
