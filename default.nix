{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  pname = "nix-build-remote";
  version = "0.1.0";
  src = ./.;

  propagatedBuildInputs = with pkgs; [
    openssh
    # for tput
    ncurses
  ];
  installPhase = ''
    mkdir -p $out/bin
    cp nix-build-remote.sh $out/bin/nix-build-remote
  '';

  # It doesn't seem possible to use containers in the build sandbox.
  doCheck = false;
  # doCheck = true;
  # postPatch = ''
    # patchShebangs ./test.sh
  # '';
  # checkInputs = with pkgs; [
    # lxd
    # nixos-generators
    # shunit2
  # ];
  # preCheck = builtins.readFile ./dev/shellhook.sh;
  # checkPhase = ''
    # runHook preCheck
    # ./test.sh
  # '';
}
