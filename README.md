A nix-build wrapper for ad hoc, backgrounded remote builds.

A typical [distributed builds](https://nixos.org/manual/nix/stable/#chap-distributed-builds) setup will run all builds remotely over an active ssh session. Sometimes you just want to run particular long-running builds remotely without necessarily keeping your development machine connected. This tool addresses this use case by invoking the build on the remote machine itself and later copying back the build result, as described in [prior art](#Prior Art).

Demo
----

```console
$ nix-build-remote -E '(with import <nixpkgs> { }; runCommand "foo" {} "sleep 5 && echo NTOYTwoi5XNPS > $out")'
Copying closure to remote...
Beginning build on remote...
these derivations will be built:
/nix/store/fnk4vmr78xx4lz23f4dmydcdqqkkvh5q-foo.drv
building '/nix/store/fnk4vmr78xx4lz23f4dmydcdqqkkvh5q-foo.drv'...

$ nix-build-remote -E '(with import <nixpkgs> { }; runCommand "foo" {} "sleep 5 && echo NTOYTwoi5XNPS > $out")'
Output already built on remote. Copying to host...
copying 1 paths...
copying path '/nix/store/8hjiwk0jz2shkzcs1zx6r930gdsxg3z7-foo' from 'ssh://alice@10.162.201.190'...

$ cat result
NTOYTwoi5XNPS
```

Installation
------------

```nix
(import (builtins.fetchTarball https://gitlab.com/ryneeverett/nix-build-remote/-/archive/master/nix-build-remote-master.tar.gz))
```

See also the annotated requirements for each machine at `dev/{remote,local}/requirements.nix`.

Usage
-----

`nix-build-remote` supports the intersection of the CLI's of `nix-build` and `nix-instantiate`, with the following distinction: the `NIX_BUILD_REMOTE` environment variable must be set to `<user>@<address>` where `<address>` is a network endpoint which can function as a remote builder and `<user>` is a user on that machine that can run builds via ssh.

Development
-----------

LXD is used for development and must be installed on the system. On nixos you can configure this with:

```nix
virtualisation.lxd.enable = true;
```

You can then enter a development environment with `nix-shell` (impure) or `nix-shell --pure --keep NIX_PATH` (NIX_PATH needed by nixos-generators) or `nix flake develop` (requires [flake support](https://zimbatm.com/NixFlakes)). This will spin up a container to function as the remote builder and add the following convenience functions to your shell:

- `nix-build-remote-dev`: invokes `nix-build-remote` within the development host container. Same arguments as `nix-build`.
- `generate-example`: echo's a unique build expression to the console because making them up gets old. The first optional argument is the time in seconds of the build and the second is the string that makes the derivation unique.
- `remote-shell`: invokes arguments on remote container. If no arguments are passed you are dropped into a shell in the remote container. *Example: remote-shell whoami.*
- `local-shell`: invokes arguments on the local container. If no arguments are passed you are dropped into a shell on the local container.

Prior Art
---------
- https://discourse.nixos.org/t/about-remote-build-machine/1029
- https://gist.github.com/danbst/09c3f6cd235ae11ccd03215d4542f7e7

Future Work
-----------

Ideally this behavior would be integrated into nix-build, but I think we'd want a way to smartly configure certain small derivations to be built locally. Ultimately I think the best way would be to integrate [build time estimates](https://github.com/NixOS/nix/issues/2763) into nix and then a configuration for what length builds (in seconds) should be done remotely.
