{...}: {
  nix = {
    # Remote signing key must be listed.
    binaryCachePublicKeys = [ "remote:qbwlW38hay5NiWXVQf07fqIpXk+dmYeev5aUib9j2uQ=" ];

    # User running nix-build-remote must be trusted in order to copy in built paths. See https://github.com/NixOS/nix/issues/2450.
    trustedUsers = [ "bob" ];
  };
}
