{pkgs, ...}: {
  imports = [ ./requirements.nix ];

  networking.hostName = "local-container";
  services.openssh = {
    enable = true;
    extraConfig = ''
      AcceptEnv NIX_BUILD_REMOTE
    '';
  };
  users.users.bob = {
    isNormalUser = true;
    openssh.authorizedKeys.keyFiles = [ ./sshkey.pub ];
  };
  nix.useSandbox = false;
  environment.etc."remote_sshkey".source = ../remote/sshkey;
  environment.systemPackages = with pkgs; [
    expect
  ];
}
