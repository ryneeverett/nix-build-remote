#!/usr/bin/env sh

export PATH="$PATH:$PWD/dev/bin";

stage() {
  echo "$(tput bold)$*...$(tput sgr0)"
}

stage "Initializing storage and network"
lxc storage create nix-build-remote-storage dir
lxc network create lxdbr0

# The above give non-zero exit codes if the storage/network already exist.
set -e

stage "Building images"
metaimg=$(nixos-generate -f lxc-metadata)
remoteimg="nix-build-remote-dev-remote"
localimg="nix-build-remote-dev-local"
buildImage() {
  name="$1"
  buildpath="$2"
  echo "Building $name image..."
  img=$(PWD=$buildpath nixos-generate -c "$buildpath/configuration.nix" -f lxc)
  lxc image delete "$name" || echo true
  lxc image import --alias "$name" "$metaimg" "$img"
}
buildImage "$remoteimg" "$PWD/dev/remote"
buildImage "$localimg" "$PWD/dev/local"

stage "Starting containers"
startContainer() {
  name="$1"
  image="$2"
  lxc stop "$name" || true
  lxc delete "$name" || true
  lxc launch "$image" "$name" --ephemeral --storage=nix-build-remote-storage
  lxc network attach lxdbr0 "$name"
}
startContainer "$remoteimg-instance" "$remoteimg"
startContainer "$localimg-instance" "$localimg"

stage "Mounting code into local container"
lxc config device add "$localimg-instance" nix-build-remote-src disk source="$PWD" path=/home/bob/nix-build-remote

stage "Defining variables"
NIX_SSHOPTS="-o SendEnv=NIX_BUILD_REMOTE -o StrictHostKeyChecking=accept-new -o UserKnownHostsFile=/dev/null -o LogLevel=QUIET"
export NIX_SSHOPTS
NIX_BUILD_REMOTE="alice@$(get-container-ip $remoteimg-instance)"
export NIX_BUILD_REMOTE
LOCAL=$(get-container-ip $localimg-instance)
export LOCAL
LOCAL_USER="bob"
export LOCAL_USER
NIX_BUILD_REMOTE_ROOTDIR="$(pwd)"
export NIX_BUILD_REMOTE_ROOTDIR
echo "Launched: remote builder => $NIX_BUILD_REMOTE, local host => $LOCAL"

stage "Configuring containers"
# shellcheck disable=SC2016
local-shell 'mkdir -p ~/.ssh && cat << EOF > ~/.ssh/config
Host $(printenv NIX_BUILD_REMOTE | sed '\''s/^.*@//'\'')
    IdentityFile /etc/remote_sshkey

StrictHostKeyChecking=accept-new
EOF'

set +e
