{...}: {
  # SSH must be running.
  services.openssh.enable = true;

  # Must have a user you can ssh into, be it via password or keyfile.
  users.users.alice = {
    isNormalUser = true;

    # either/or
    password = "obscurity";
    openssh.authorizedKeys.keyFiles = [ ./sshkey.pub ];
  };

  nix = {
    # There must be a signing key configured in order for the built path to be copyable across machines.
    extraOptions = ''
      secret-key-files = /etc/signingkey.pem
    '';
    # The user you have ssh access to must be trusted in order to sign the path.
    trustedUsers = [ "alice" ];
  };
}
