{pkgs, ...}: {
  imports = [ ./requirements.nix ];

  networking.hostName = "remote-container";
  nix.useSandbox = false;
  environment.etc."signingkey.pem".source = ./signingkey.pem;
}
