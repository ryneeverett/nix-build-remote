{
  description = "nix-build-remote";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {

      packages.nix-build-remote = import ./. { inherit pkgs; };

      defaultPackage = self.packages.${system}.nix-build-remote;

      devShell = import ./shell.nix { inherit pkgs; };

    });
}
